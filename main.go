package main

import (
	_ "embed"
	"fmt"
	"gitlab.com/evolves-fr/gommon"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"text/template"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"

	//go:embed sw.js.gotxt
	serviceWorkerTemplate string
)

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true})
}

func main() {
	cli.VersionPrinter = func(ctx *cli.Context) {
		fmt.Printf("%s (%s - %s)\n", version, commit, date)
	}

	flags := []cli.Flag{
		&cli.BoolFlag{
			Name:    "debug",
			Usage:   "Enable debug mode",
			Aliases: []string{"d"},
			EnvVars: []string{"HUGO_SW_DEBUG"},
		},
		&cli.StringFlag{
			Category: "Service worker",
			Name:     "public",
			Usage:    "Public folder dist",
			Aliases:  []string{"p"},
			EnvVars:  []string{"HUGO_SW_PUBLIC"},
			Value:    "./public/",
		},
		&cli.StringFlag{
			Category: "Service worker",
			Name:     "output",
			Usage:    "Destination service worker filepath",
			Aliases:  []string{"o"},
			EnvVars:  []string{"HUGO_SW_OUTPUT"},
			Value:    "./public/service-worker.js",
		},
		&cli.StringFlag{
			Category: "Service worker",
			Name:     "cacheName",
			Usage:    "Prefix cache name of service worker (suffixes with timestamp)",
			Aliases:  []string{"n"},
			EnvVars:  []string{"HUGO_SW_CACHE_NAME"},
			Value:    "cache",
		},
		&cli.StringSliceFlag{
			Category: "Service worker",
			Name:     "ignore",
			Usage:    "Ignored files list of offline cache",
			Aliases:  []string{"i"},
			EnvVars:  []string{"HUGO_SW_IGNORED_FILES"},
			FilePath: ".swignore",
		},
	}

	app := &cli.App{
		Name:        "hugo-sw",
		Usage:       "Hugo - Service worker generator",
		UsageText:   "hugo-sw [options]",
		Description: "Generate service-worker.js from Hugo project.",
		Version:     version,
		Copyright:   "Evolves SAS © 2022",
		Authors:     []*cli.Author{{Name: "Thomas FOURNIER", Email: "tfournier@evolves.fr"}},
		Flags:       flags,
		Before:      prepare,
		Action:      generateServiceWorker,
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}

func prepare(ctx *cli.Context) error {
	if ctx.Bool("debug") {
		logrus.SetLevel(logrus.DebugLevel)
	}

	return nil
}

func generateServiceWorker(ctx *cli.Context) error {
	return serviceWorker{
		public:       ctx.String("public"),
		output:       ctx.String("output"),
		cacheName:    ctx.String("cacheName"),
		ignoredFiles: gommon.Uniq(gommon.Compact(gommon.SplitSlice[string](ctx.StringSlice("ignore"), "\n"))),
	}.generate()
}

type serviceWorker struct {
	public       string
	output       string
	cacheName    string
	ignoredFiles []string
}

func (s serviceWorker) cacheResources() ([]string, error) {
	var contents = make([]string, 0)

	if err := filepath.WalkDir(s.public, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		// Ignore directory, public path and hidden files
		if d.IsDir() || path == filepath.Clean(s.output) || strings.HasPrefix(d.Name(), ".") {
			logrus.Debugf("Skip %s", path)
			return nil
		}

		// Trim public folder path
		path = strings.TrimPrefix(path, filepath.Dir(s.public))

		// Ignore files registered in config
		for _, ignored := range s.ignoredFiles {
			if path == ignored {
				logrus.Debugf("Ignore %s", path)
				return nil
			}
		}

		// Transform path to rewrite path
		if strings.HasSuffix(path, "index.html") || strings.HasSuffix(path, ".html") {
			logrus.Debugf("Rewrite %s", path)
			switch {
			case strings.HasSuffix(path, "index.html"):
				path = strings.TrimSuffix(path, "index.html")
			case strings.HasSuffix(path, ".html"):
				path = strings.TrimSuffix(path, ".html")
			}
		}

		logrus.Debugf("Use %s", path)
		contents = append(contents, path)

		return nil
	}); err != nil {
		return nil, err
	}

	return contents, nil
}

func (s serviceWorker) generate() error {
	cacheResources, err := s.cacheResources()
	if err != nil {
		return err
	}

	swFile, err := os.OpenFile(s.output, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0o644)
	if err != nil {
		return err
	}

	tpl, err := template.New(filepath.Base(s.output)).Parse(serviceWorkerTemplate)
	if err != nil {
		return err
	}

	var data = map[string]any{
		"cacheName":     fmt.Sprintf("%s-%d", s.cacheName, time.Now().UTC().Unix()),
		"cacheResource": cacheResources,
	}

	return tpl.Execute(swFile, data)
}

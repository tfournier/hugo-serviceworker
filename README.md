# Hugo ServiceWorker Generator

## Usage

```
NAME:
   hugo-sw - Hugo - Service worker generator

USAGE:
   hugo-sw [options]

DESCRIPTION:
   Generate service-worker.js from Hugo project.

AUTHOR:
   Thomas FOURNIER <tfournier@evolves.fr>

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --debug, -d    Enable debug mode (default: false) [$HUGO_SW_DEBUG]
   --help, -h     show help (default: false)
   --version, -v  print the version (default: false)

   Service worker
   --cacheName value, -n value                            Prefix cache name of service worker (suffixes with timestamp) (default: "cache") [$HUGO_SW_CACHE_NAME]
   --ignore value, -i value [ --ignore value, -i value ]  Ignored files list of offline cache [$HUGO_SW_IGNORED_FILES]
   --output value, -o value                               Destination service worker filepath (default: "./public/service-worker.js") [$HUGO_SW_OUTPUT]
   --public value, -p value                               Public folder dist (default: "./public/") [$HUGO_SW_PUBLIC]


COPYRIGHT:
   Evolves SAS © 2022
```

Ignored files list can set in `.swignore` file. 

## Examples

```shell
hugo-sw --cacheName "myApp"
```

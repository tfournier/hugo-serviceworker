.PHONY: build
build:
	goreleaser build --rm-dist

.PHONY: snapshot
snapshot:
	goreleaser release --rm-dist --snapshot

.PHONY: release
release:
	goreleaser release --rm-dist
